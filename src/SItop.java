import java.awt.Graphics;

/**
 * This class constructs the top layer of invaders
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
public class SItop extends SIinvader {

    /**
     * This is the constructor for SItop
     * 
     * @param x
     *            coordinate of the invader
     * @param y
     *            coordinate of the invader
     */
    public SItop(int x, int y) {
        super(x, y, 0, 0, "SItop0.gif", "SItop1.gif", "SIinvaderBlast.gif");
        super.setHeight(super.getImage("SItop0.gif").getHeight(null));
        super.setWidth(super.getImage("SItop0.gif").getWidth(null));
    }

    /**
     * draws image 1 of the invader to the screen
     */
    @Override
    public void draw(Graphics in) {
        in.drawImage(super.getImageOne(), super.getX(), super.getY(), null);
    }

    /**
     * draws image 2 of the invader to the screen
     */
    public void drawSecondImage(Graphics in) {
        in.drawImage(super.getImageTwo(), super.getX(), super.getY(), null);
    }

    /**
     * draws the destroyed image of the invader to the screen
     */
    public void drawDestroyed(Graphics in) {
        in.drawImage(super.getDestroyImage(), super.getX(), super.getY(),
                null);

    }

}
