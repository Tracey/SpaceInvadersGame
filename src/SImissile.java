import java.awt.Color;
import java.awt.Graphics;

/**
 * This class creates a missile object for both the base and the invaders to
 * shoot
 * 
 * @author tyler_tracey
 * @version 1.1.2
 *
 */
public class SImissile extends SIthing {

    /**
     * This is the constructor class for missile, dimensions of the object are
     * preset to 2 by 10 pixels
     * 
     * @param x
     *            coordinate on the screen to shoot from
     * @param y
     *            coordinate on the screen to shoot from
     */
    protected SImissile(int x, int y) {
        super(x, y, 2, 10);
    }

    /**
     * method draws the missile to the panel
     */
    @Override
    public void draw(Graphics g) {
        g.fillRect(getX(), getY(), 2, 10);
        g.setColor(Color.WHITE);

    }
}