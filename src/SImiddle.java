import java.awt.Graphics;

/**
 * This class constructs the middle layer of invaders
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
public class SImiddle extends SIinvader {

    /**
     * This is the constructor for SImiddle
     * 
     * @param x
     *            coordinate of the invader
     * @param y
     *            coordinate of the invader
     */
    public SImiddle(int x, int y) {
        super(x, y, 0, 0, "SImiddle0.gif", "SImiddle1.gif",
                "SIinvaderBlast.gif");
        super.setHeight(super.getImage("SImiddle0.gif").getHeight(null));
        super.setWidth(super.getImage("SImiddle0.gif").getWidth(null));
    }

    /**
     * This method draws image 1 of the invader
     */
    @Override
    public void draw(Graphics in) {
        in.drawImage(super.getImageOne(), super.getX(), super.getY(), null);

    }

    /**
     * this method draws image 2 of the invader
     */
    public void drawSecondImage(Graphics in) {
        in.drawImage(super.getImageTwo(), super.getX(), super.getY(), null);
    }

    /**
     * this method draws the destroyed image of the invader
     */
    public void drawDestroyed(Graphics in) {
        in.drawImage(super.getDestroyImage(), super.getX(), super.getY(),
                null);

    }

}
