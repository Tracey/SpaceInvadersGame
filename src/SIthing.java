import java.awt.Graphics;

/**
 * This abstract class is used to set and edit the coordinates and size of each
 * object on the screen
 * 
 * @author traceys5
 * @version 1.1.1
 *
 */
public abstract class SIthing {

    private int x;
    private int y;
    private int width;
    private int height;

    /**
     * This constructor takes in the coordinates of an object on the screen as
     * well as its dimensions
     * 
     * @param x
     *            is the x coordinate of the object
     * @param y
     *            is the y coordinate of the object
     * @param width
     *            is the width of the object
     * @param height
     *            is the height of the object
     */
    protected SIthing(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * This abstract method paints the objects onto the screen
     * 
     * @param in
     *            is the graphic to be added
     */
    public abstract void draw(Graphics in);

    /**
     * This method returns the x coordinated of the object
     * 
     * @return x integer value
     */
    protected int getX() {
        return x;
    }

    /**
     * This method returns the y coordinate of the object
     * 
     * @return y integer value
     */
    protected int getY() {
        return y;
    }

    /**
     * This method sets the x coordinate of the object
     * 
     * @param x
     *            integer value
     */
    protected void setX(int x) {
        this.x = x;
    }

    /**
     * This method sets the y coordinate of the object
     * 
     * @param y
     *            integer y value
     */
    protected void setY(int y) {
        this.y = y;
    }

    /**
     * This method determines if the object is able to move any farther to the
     * right of the frame, does so if true
     * 
     * @param x
     *            is the value trying to be moved
     * @return boolean true if it can be moved x amount of spaces to the right,
     *         false otherwise
     */
    public boolean moveRight(int x) {
        if (this.x + x < 500) {
            this.x += x;
            return true;
        }
        return false;
    }

    /**
     * This method determines if the object is able to move any farther to the
     * left of the frame, does so if true
     * 
     * @param x
     *            is the value trying to be moved
     * @return boolean true if it can be moved x amount of spaces to the left,
     *         false otherwise
     */
    public boolean moveLeft(int x) {
        if (this.x + x > 0) {
            this.x -= x;
            return true;
        }
        return false;
    }

    /**
     * This method determines if the object is able to move any farther up the
     * frame, does so if true
     * 
     * @param x
     *            is the value trying to be moved
     * @return boolean true if it can be moved x amount of spaces upward, false
     *         otherwise
     */
    public boolean moveUp(int y) {
        if (this.y + y < 450) {
            this.y += y;
            return true;
        }
        return false;
    }

    /**
     * This method determines if the object is able to move any farther down the
     * frame, does so if true
     * 
     * @param x
     *            is the value trying to be moved
     * @return boolean true if it can be moved x amount of spaces downward,
     *         false otherwise
     */
    public boolean moveDown(int y) {
        if (this.y + y > 0) {
            this.y -= y;
            return true;
        }
        return false;
    }

    /**
     * This method returns the width of the object
     * 
     * @return width of the object
     */
    public int getWidth() {
        return width;
    }

    /**
     * This method returns the height of the object
     * 
     * @return height of the object
     */
    public int getHeight() {
        return height;
    }

    /**
     * This method sets the width of the object
     * 
     * @param width
     *            to set the object to
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * This method sets the height of the object
     * 
     * @param height
     *            to set the object to
     */
    public void setHeight(int height) {
        this.height = height;
    }
}