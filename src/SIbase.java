import java.applet.AudioClip;
import java.awt.Graphics;

/**
 * This class creates the base ship instance and all of its possible states
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
public class SIbase extends SIship {

    private AudioClip baseShootSound = super.getBaseShootSound();

    /**
     * This is the constructor for SIbase
     */
    public SIbase() {
        // makes the base object in the super ship class
        super(250, 360, 0, 0, "SIbase.gif", "SIbase.gif", "SIbaseBlast.gif");
        // sets the height of the ship in SIthing by getting the image from
        // SIship
        super.setHeight(super.getImage("SIbase.gif").getHeight(null));
        // sets the width of the ship in SIthing by getting the image from
        // SIship
        super.setWidth(super.getImage("SIbase.gif").getWidth(null));

    }

    /**
     * This method returns the base shoot sound
     * 
     * @return base shoot sound
     */
    public AudioClip getShootSound() {
        return baseShootSound;
    }

    /**
     * This class draws the base ship onto the screen
     * 
     */
    @Override
    public void draw(Graphics in) {
        in.drawImage(super.getImageOne(), super.getX(), super.getY(), null);
    }

}