import java.awt.Graphics;

/**
 * This class constructs the bottom layer of invaders
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
public class SIbottom extends SIinvader {

    /**
     * This is the constructor for SIbottom
     * 
     * @param x
     *            coordinate of the invader
     * @param y
     *            coordinate of the invader
     */
    public SIbottom(int x, int y) {
        // makes the invaders in the super ship class
        super(x, y, 0, 0, "SIbottom0.gif", "SIbottom1.gif",
                "SIinvaderBlast.gif");
        // sets the height of the ship in SIthing by getting the image from
        // SIship
        super.setHeight(super.getImage("SIbottom0.gif").getHeight(null));
        // sets the width of the ship in SIthing by getting the image from
        // SIship
        super.setWidth(super.getImage("SIbottom0.gif").getWidth(null));
    }

    /**
     * Draws image 1 of the invader to the screen
     */
    @Override
    public void draw(Graphics in) {
        in.drawImage(super.getImageOne(), super.getX(), super.getY(), null);
    }

    /**
     * draws image 2 of the invader to the screen
     */
    public void drawSecondImage(Graphics in) {
        in.drawImage(super.getImageTwo(), super.getX(), super.getY(), null);
    }

    /**
     * draws the destroyed image of the invader to the screen
     */
    public void drawDestroyed(Graphics in) {
        in.drawImage(super.getDestroyImage(), super.getX(), super.getY(),
                null);
    }

}