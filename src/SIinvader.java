import java.awt.Graphics;
import java.util.Random;

/**
 * This abstract class creates and holds the point values associated with the
 * invader ships
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
public abstract class SIinvader extends SIship {

    Random rand = new Random();
    String invader;

    /**
     * This constructor constructs an invader ship and its position on the
     * screen
     * 
     * @param posX
     *            x coordinate on the screen
     * @param posY
     *            y coordinate on the screen
     * @param width
     *            value of the width of the ship
     * @param height
     *            value of the height of the ship
     * @param image1File
     *            string filename for the first image instance of the ship
     * @param image2File
     *            string filename for the second image instance of the ship
     * @param imageDeadFile
     *            string filename for the destroyed image of the ship
     */
    public SIinvader(int posX, int posY, int width, int height,
            String image1File, String image2File, String imageDestroyFile) {
        super(posX, posY, width, height, image1File, image2File,
                imageDestroyFile);

        invader = image1File;
    }

    /**
     * This method draws the first image instance of the invader
     * 
     * @param in
     *            is the image graphic
     */
    
    @Override
    public void draw(Graphics in) {

    }

    /**
     * This method draws the second image instance of the invader
     * 
     * @param in
     *            is the image graphic
     */
    public void drawSecondImage(Graphics in) {

    }

    /**
     * This method draws the destroyed image instance of the invader
     * 
     * @param in
     *            is the image graphic
     */
    public void drawDestroyed(Graphics in) {

    }

    /**
     * This method returns the point values associated with each invader,
     * including the mystery invader
     * 
     * @return points to be awarded upon destroying the invader ship
     */
    public int getPoints() {

        if (invader == "SIbottom0.gif" || invader == "SIbottom1.gif") {
            return 10;
        }

        else if (invader == "SImiddle0.gif" || invader == "SImiddle1.gif") {
            return 20;
        }

        else if (invader == "SItop0.gif" || invader == "SItop1.gif") {
            return 30;
        }

        // The mystery ship points
        else {
            int random = rand.nextInt((4 - 1) + 1);
            switch (random) {
                case 1:
                    return 300;
                case 2:
                    return 150;
                case 3:
                    return 100;
                default:
                    return 50;
            }
        }
    }

}