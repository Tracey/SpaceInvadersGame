import java.awt.Graphics;

/**
 * This class holds the mystery ship
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
public class SImystery extends SIinvader {

    /**
     * This is the constructor for the mystery invader
     * 
     * @param x
     *            coordinate of the mystery invader,
     */
    public SImystery(int x) {
        super(x, 20, 0, 0, "SIMystery.gif", "SIMystery.gif",
                "SIinvaderBlast.gif");
        super.setHeight(super.getImage("SIMystery.gif").getHeight(null));
        super.setWidth(super.getImage("SIMystery.gif").getWidth(null));
    }

    /**
     * This method draws the mystery invader
     */
    @Override
    public void draw(Graphics g) {
        g.drawImage(super.getImageOne(), super.getX(), super.getY(), null);

    }
}
