import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 * This abstract class constructs the type of ship and all components that are
 * associated with the ship object
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
public abstract class SIship extends SIthing {

    private Image image1;
    private Image image2;
    private Image destroyImage;
    private AudioClip destroySound;
    private AudioClip mysteryDestroySound;
    private AudioClip baseShootSound;

    /**
     * This constructor creates a ship object, including its position on the
     * screen
     * 
     * @param posX
     *            is the current x coordinate position
     * @param posY
     *            is the current y coordinate position
     * @param width
     *            is the width of the image
     * @param height
     *            is the height of the image
     * @param image1File
     *            the file name of the first ship image
     * @param image2File
     *            the file name of the second ship image
     * @param destroyImageFile
     *            the file name of the destroyed ship image
     */
    public SIship(int posX, int posY, int width, int height, String image1File,
            String image2File, String destroyImageFile) {

        super(posX, posY, width, height);
        this.image1 = getImage(image1File);
        this.image2 = getImage(image2File);
        this.destroyImage = getImage(destroyImageFile);
        this.destroySound = getSound("SIshipHit.wav");
        this.mysteryDestroySound = getSound("SImystery.wav");
        this.baseShootSound = getSound("SIbaseShoot.wav");
    }

    /**
     * This method returns the first ship image
     * 
     * @return the image of the first ship instance
     */
    protected Image getImageOne() {
        return image1;
    }

    /**
     * This method returns the second ship image
     * 
     * @return the image of the second ship instance
     */
    protected Image getImageTwo() {
        return image2;
    }

    /**
     * this method returns the image when the ship is destroyed
     * 
     * @return the image of the destroyed ship instance
     */
    protected Image getDestroyImage() {
        return destroyImage;
    }

    /**
     * this method returns sound of the ship being destroyed
     * 
     * @return the audioClip when the ship is destroyed
     */
    protected AudioClip getDestroySound() {
        return destroySound;
    }

    /**
     * This method returns the base shoot sound
     * 
     * @return base shoot audio
     */
    protected AudioClip getBaseShootSound() {
        return baseShootSound;
    }

    /**
     * this method returns the sound of the mystery ship being destroyed
     * 
     * @return the auidioCLip of the mystery ship being destroyed
     */
    protected AudioClip getMysteryDestroySound() {
        return mysteryDestroySound;
    }

    /**
     * This method processes the file name of the ship image and returns the
     * actual image
     * 
     * @param fileName
     *            String of the imported file name
     * @return the image that the filename held
     */
    protected Image getImage(String fileName) {
        URL file = getClass().getResource(fileName);
        ImageIcon icon = new ImageIcon(file);
        return icon.getImage();
    }

    /**
     * This method processes the audio file string name and stores the audioClip
     * it held
     * 
     * @param filename
     *            String name of the audio file
     * @return the audioFile of the file that was input
     */
    protected AudioClip getSound(String filename) {
        URL file = getClass().getResource(filename);
        return Applet.newAudioClip(file);
    }

}