import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * This GUI class controls the main frame of the space invaders game
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
@SuppressWarnings("serial")
public class SI extends JFrame {

    public SI() {
        super("Space Invaders");
        final SIpanel panel = new SIpanel();
        add(panel);
        setSize(500, 450);

        // Adding the menu bar to the JFrame
        JMenuBar menubar = new JMenuBar();
        setJMenuBar(menubar);

        // Adding the menu components to the JFrame menu bar
        JMenu game = new JMenu("Game");
        menubar.add(game);
        JMenu help = new JMenu("Help");
        // Aligns the "Help" menu to the right side of the menu bar
        menubar.add(Box.createHorizontalGlue());
        menubar.add(help);

        // Adding the menu items to the menu components
        JMenuItem newGame = new JMenuItem("New Game");
        game.add(newGame);
        JMenuItem pause = new JMenuItem("Pause");
        game.add(pause);
        JMenuItem resume = new JMenuItem("Resume");
        game.add(resume);
        JMenuItem quit = new JMenuItem("Quit");
        game.add(quit);
        JMenuItem about = new JMenuItem("About...");
        help.add(about);

        // setting the resume option to false
        resume.setEnabled(false);

        // About menu Action Listener
        about.addActionListener(new ActionListener() {
            @SuppressWarnings("static-access")
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane aboutMe = new JOptionPane();
                aboutMe.showMessageDialog(null, new JLabel(
                        "<html><hr><i>Space Invaders</i><br>by Tyler Tracey<hr></html>"));
            }
        });

        // NewGame Action Listener
        newGame.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int result = JOptionPane.showConfirmDialog(SI.this,
                        "Start a new game?");
                if (result == JOptionPane.YES_OPTION) {
                    panel.resetScore();
                    panel.newGame();
                    resume.setEnabled(false);
                    pause.setEnabled(true);
                }
            }
        });

        // Pause Action Listener
        pause.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                panel.pause();
                pause.setEnabled(false);
                resume.setEnabled(true);
            }
        });

        // Resume Action Listener
        resume.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                panel.resume();
                resume.setEnabled(false);
                pause.setEnabled(true);
            }
        });

        // Quit Action Listener
        quit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int result = JOptionPane.showConfirmDialog(SI.this,
                        "Are you sure you want to quit?");
                if (result == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });
        setSize(500, 450);
        setResizable(false);
    }

    // Main class that runs the game
    public static void main(String args[]) {
        SI test = new SI();
        test.setVisible(true);
    }

}
