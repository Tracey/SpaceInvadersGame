import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class SIpanel extends JPanel {
    // Game timer
    private Timer timer;

    // Integer Fields
    int imageCount = 0;
    int x = 40;
    int y = 40;
    int score = 0;
    int invaderDirection = 1;
    int invaderWidth = 24;
    int invaderHeight = 30;
    int counter = 0;
    int test = 0;
    int invaderMove = 5;
    int mysteryMove = 1;
    int mysteryEvent;
    int invaderCount = 50;
    int idc;

    // invader speed multiplier
    double speed = 1.0;

    // Object fields
    private SIbase baseShip;
    private SImystery mysteryShip;
    private SImissile baseMissile;
    private SImissile invaderMissile1;
    private SImissile invaderMissile2;
    private SImissile invaderMissile3;

    // Rectangle fields
    private Rectangle baseRect = new Rectangle(0, 0, 0, 0);
    private Rectangle baseMissileRect = new Rectangle(0, 0, 0, 0);
    private Rectangle invaderMissileRect1 = new Rectangle(0, 0, 0, 0);
    private Rectangle invaderMissileRect2 = new Rectangle(0, 0, 0, 0);
    private Rectangle invaderMissileRect3 = new Rectangle(0, 0, 0, 0);
    private Rectangle invaderRect = new Rectangle(0, 0, 0, 0);
    private Rectangle mysteryRect = new Rectangle(0, 0, 0, 0);

    // Boolean fields
    private boolean baseFired = false;
    private boolean invaderFired1 = false;
    private boolean invaderFired2 = false;
    private boolean invaderFired3 = false;
    private boolean baseDestroyed = false;
    private boolean invaderDestroyed = false;
    private boolean mysteryDestroyed = false;
    private boolean mysteryAlive = false;
    private boolean paused = false;

    // Random
    Random r = new Random();

    // ArrayList of invaders
    private ArrayList<SIinvader> enemies;

    /**
     * The constructor for the game panel
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public SIpanel() {
        enemies = new ArrayList();
        drawBase();
        addInvaders();
        setBackground(Color.BLACK);
        setFocusable(true);

        timer = new Timer(10, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                counter++;
                imageCount++;
                updateGame();
                repaint();
            }
        });
        timer.start();
        addKeyListener(new KeyListener() {

            public void keyPressed(KeyEvent e) {
                if (!paused) {
                    switch (e.getKeyCode()) {

                        case KeyEvent.VK_SPACE:
                            if (!baseFired) {
                                baseMissile = new SImissile(
                                        baseShip.getX() + 12,
                                        baseShip.getY() - 26);
                                baseFired = true;
                            }
                            break;

                        case KeyEvent.VK_LEFT:
                            if (baseShip.getX() > 0) {
                                baseShip.setX(baseShip.getX() - 10);
                            }
                            break;
                        case KeyEvent.VK_RIGHT:
                            if (baseShip.getX() < 450) {
                                baseShip.setX(baseShip.getX() + 10);
                            }
                    }
                }
            }

            public void keyReleased(KeyEvent arg0) {
            }

            public void keyTyped(KeyEvent e) {
                e.getKeyChar();
                repaint();
            }
        });
    }

    public void updateGame() {
        if (baseFired) {
            baseMissileRect = new Rectangle(baseMissile.getX(),
                    baseMissile.getY(), baseMissile.getWidth(),
                    baseMissile.getHeight());
            y = baseMissile.getY();
            baseMissile.setY(y -= 5);
            if (baseMissile.getY() < 0) {
                baseMissile = null;
                baseFired = false;
            }
        }
        if (counter == Math.round(40 / speed)) {
            invaderDirection = invaderDirection * -1;
            for (int i = 0; i < invaderCount; i++) {
                int random = r.nextInt(4);
                if (random == 1 || random == 2 || random == 3) {
                    if (invaderFired1 == false) {
                        invaderMissile1 = new SImissile(enemies.get(i).getX(),
                                enemies.get(i).getY());
                        invaderFired1 = true;
                    }
                    else if (invaderFired2 == false) {
                        invaderMissile2 = new SImissile(enemies.get(i).getX(),
                                enemies.get(i).getY());
                        invaderFired2 = true;
                    }
                    else if (invaderFired3 == false) {
                        invaderMissile3 = new SImissile(enemies.get(i).getX(),
                                enemies.get(i).getY());
                        invaderFired3 = true;
                    }
                }
                enemies.get(i).setX(enemies.get(i).getX() + invaderMove);
                if (enemies.get(i).getX() < 0 || enemies.get(i).getX() > 450) {
                    invaderMove = invaderMove * -1;
                    for (int k = 0; k < invaderCount; k++) {
                        enemies.get(k).setY(enemies.get(k).getY() + 12);
                    }
                    speed += .2;
                }
                if (enemies.get(0).getY() > 330) {
                    baseDestroyed = true;
                }
            }
            counter = 0;
        }
        if (counter % 2 == 0) {
            if (invaderFired1 == true) {
                invaderMissile1.setY(invaderMissile1.getY() + 5);
                if (invaderMissile1.getY() > 450) {
                    invaderMissile1 = null;
                    invaderFired1 = false;
                }
            }
            if (invaderFired2 == true) {
                invaderMissile2.setY(invaderMissile2.getY() + 5);
                if (invaderMissile2.getY() > 450) {
                    invaderMissile2 = null;
                    invaderFired2 = false;
                }
            }
            if (invaderFired3 == true) {
                invaderMissile3.setY(invaderMissile3.getY() + 5);
                if (invaderMissile3.getY() > 450) {
                    invaderMissile3 = null;
                    invaderFired3 = false;
                }
            }
            if (mysteryAlive == true) {
                mysteryShip.setX(mysteryShip.getX() + mysteryMove);
                if (mysteryShip.getX() < 0) {
                    mysteryMove = 1;
                }
                if (mysteryShip.getX() > 450) {
                    mysteryMove = -1;
                }
            }
        }
        for (idc = 0; idc < invaderCount; idc++) {
            invaderRect = new Rectangle(enemies.get(idc).getX(),
                    enemies.get(idc).getY(), enemies.get(idc).getWidth(),
                    enemies.get(idc).getHeight());

            if (baseFired == true) {
                if (baseMissileRect.intersects(invaderRect)) {
                    invaderDestroyed = true;
                    baseMissile = null;
                    baseFired = false;
                    baseMissileRect = null;
                    invaderCount--;
                    break;
                }
            }
        }
        if (mysteryAlive == true && baseFired == true
                && baseMissileRect.intersects(mysteryRect)) {
            mysteryDestroyed = true;
            baseMissileRect = null;
            mysteryAlive = false;
        }
        if ((invaderFired1 == true && invaderMissileRect1.intersects(baseRect))
                || (invaderFired2 && invaderMissileRect2.intersects(baseRect))
                || (invaderFired3
                        && invaderMissileRect3.intersects(baseRect))) {
            baseDestroyed = true;
        }
        mysteryEvent = r.nextInt(1001);
        if (mysteryAlive == false && mysteryEvent >= 998) {
            mysteryEvent = r.nextInt(3);
            if (mysteryEvent == 1) {
                mysteryShip = new SImystery(450);
            }
            else {
                mysteryShip = new SImystery(0);
            }
            mysteryAlive = true;
        }
    }

    /**
     * This method paints the components of the game on to the screen and also
     * outputs the sound
     */
    @Override
    protected void paintComponent(Graphics in) {
        super.paintComponent(in);
        Font font = new Font("Times New Roman", 20, 20);
        in.setColor(Color.GREEN);
        in.drawString("Score: " + score,
                500 - in.getFontMetrics(font).stringWidth("Score: " + score),
                10);
        in.setColor(Color.WHITE);

        if (baseFired) {
            baseMissile.draw(in);
        }
        if (invaderFired1) {
            invaderMissile1.draw(in);
            invaderMissileRect1 = new Rectangle(invaderMissile1.getX(),
                    invaderMissile1.getY(), invaderMissile1.getWidth(),
                    invaderMissile1.getHeight());
        }
        if (invaderFired2) {
            invaderMissile2.draw(in);
            invaderMissileRect2 = new Rectangle(invaderMissile2.getX(),
                    invaderMissile2.getY(), invaderMissile2.getWidth(),
                    invaderMissile2.getHeight());
        }
        if (invaderFired3) {
            invaderMissile3.draw(in);
            invaderMissileRect3 = new Rectangle(invaderMissile3.getX(),
                    invaderMissile3.getY(), invaderMissile3.getWidth(),
                    invaderMissile3.getHeight());
        }
        if (mysteryAlive) {
            mysteryShip.draw(in);
            mysteryRect = new Rectangle(mysteryShip.getX(), mysteryShip.getY(),
                    mysteryShip.getWidth(), mysteryShip.getHeight());
        }

        if (invaderDestroyed) {
            enemies.get(idc).getDestroySound().play();
            enemies.get(idc).drawDestroyed(in);
            score += enemies.get(idc).getPoints();
            enemies.remove(idc);

            invaderDestroyed = false;
        }
        if (invaderDirection > 0) {
            for (int i = 0; i < invaderCount; i++) {
                enemies.get(i).draw(in);
            }
        }
        else {
            for (int i = 0; i < invaderCount; i++) {
                enemies.get(i).drawSecondImage(in);
            }
        }
        // if the base has fired a missile
        if (baseFired == true) {
            baseShip.getBaseShootSound().play();
        }

        // if the mystery ship is hit with the base missile
        if (mysteryDestroyed == true) {
            mysteryShip.getMysteryDestroySound().play();
            in.drawImage(mysteryShip.getDestroyImage(), mysteryShip.getX(),
                    mysteryShip.getY(), mysteryShip.getWidth(),
                    mysteryShip.getHeight(), null);
            score += mysteryShip.getPoints();
            mysteryDestroyed = false;
            mysteryShip = null;
            baseMissile = null;
            baseFired = false;
        }

        // if the base is destroyed
        if (baseDestroyed == false) {
            baseShip.draw(in);
            baseRect = new Rectangle(baseShip.getX(), baseShip.getY(),
                    baseShip.getWidth(), baseShip.getHeight());
        }
        // if the invaders are all destroyed
        if (invaderCount == 0) {
            newGame();
        }
        // if the invaders got to the base ship before all being destroyed
        if (baseDestroyed == true) {
            paused = true;
            timer.stop();
            in.drawImage(baseShip.getDestroyImage(), baseShip.getX(),
                    baseShip.getY(), null);
            baseShip = null;
            in.setColor(Color.GREEN);
            in.setFont(new Font("Times New Roman", 50, 50));
            in.drawString("Game Over", 100, 225);
            baseDestroyed = false;
        }

    }

    /**
     * This method pauses the game
     */
    public void pause() {
        timer.stop();
        paused = true;
    }

    /**
     * This method resumes the game when paused
     */
    public void resume() {
        timer.start();
        paused = false;
    }

    /**
     * This method creates a new game
     */
    public void newGame() {
        enemies.clear();
        reset();
        addInvaders();
        drawBase();
        timer.restart();
    }

    /**
     * This method adds the invaders to the screen by multiplying the width of
     * the image by how many invaders are before the current, and the y value by
     * how many rows came before
     */
    public void addInvaders() {
        for (int i = 0; i < 10; i++) {
            SIbottom invader = new SIbottom((i * 35 + 50), 80 + 25 * 4);
            enemies.add(invader);
        }
        for (int i = 0; i < 10; i++) {
            SIbottom invader = new SIbottom((i * 35 + 50), 80 + 25 * 3);
            enemies.add(invader);
        }
        for (int i = 0; i < 10; i++) {
            SImiddle invader = new SImiddle((i * 35 + 50), 80 + 25 * 2);
            enemies.add(invader);
        }
        for (int i = 0; i < 10; i++) {
            SImiddle invader = new SImiddle((i * 35 + 50), 80 + 25);
            enemies.add(invader);
        }
        for (int i = 0; i < 10; i++) {
            SItop invader = new SItop((i * 35 + 50), 80);
            enemies.add(invader);
        }
    }

    /**
     * This method draws the base ship onto the screen
     */
    public void drawBase() {
        baseShip = new SIbase();
        baseRect = new Rectangle(baseShip.getX(), baseShip.getY(),
                baseShip.getWidth(), baseShip.getHeight());
    }

    /**
     * This method resets the game
     */
    public void reset() {
        paused = false;
        invaderCount = 50;
        invaderMove = 5;
        mysteryShip = null;
        mysteryAlive = false;
        baseFired = false;
        invaderFired1 = false;
        invaderFired2 = false;
        invaderFired3 = false;
        baseMissile = null;
        invaderMissile1 = null;
        invaderMissile2 = null;
        invaderMissile3 = null;
        baseDestroyed = false;
        speed = 1;
        counter = 0;
    }

    /**
     * This method resets the score
     */
    public void resetScore() {
        score = 0;
    }
}